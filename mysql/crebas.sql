/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     5/13/2019 3:46:50 PM                         */
/*==============================================================*/


drop table if exists CONVOCATION;

drop table if exists COURS;

drop table if exists COURS_FILIERE;

drop table if exists DOSSIER_SCOLAIRE;

drop table if exists ENSEIGNANT;

drop table if exists ETUDIANT;

drop table if exists FILIERE;

/*==============================================================*/
/* Table: CONVOCATION                                           */
/*==============================================================*/
create table CONVOCATION
(
   IDETUDIANT           int not null,
   IDCOURS              int not null,
   primary key (IDETUDIANT, IDCOURS)
);

/*==============================================================*/
/* Table: COURS                                                 */
/*==============================================================*/
create table COURS
(
   IDCOURS              int not null AUTO_INCREMENT,
   IDENSEIGNANT         int,
   MATIERECOURS         varchar(255),
   NIVEAUCOURS          varchar(10),
   ANNEESCOLAIRE        varchar(20),
   SALLEEXAMEN          varchar(250),
   DATEEXAMEN           date,
   primary key (IDCOURS)
);

/*==============================================================*/
/* Table: COURS_FILIERE                                         */
/*==============================================================*/
create table COURS_FILIERE
(
   IDCOURS              int not null,
   CODEFILIERE          varchar(8) not null,
   primary key (IDCOURS, CODEFILIERE)
);

/*==============================================================*/
/* Table: DOSSIER_SCOLAIRE                                      */
/*==============================================================*/
create table DOSSIER_SCOLAIRE
(
   IDETUDIANT           int not null,
   ANNEESCOLAIRE        varchar(20) not null,
   CODEFILIERE          varchar(8) not null,
   primary key (IDETUDIANT, ANNEESCOLAIRE)
);

/*==============================================================*/
/* Table: ENSEIGNANT                                            */
/*==============================================================*/
create table ENSEIGNANT
(
   IDENSEIGNANT         int not null AUTO_INCREMENT,
   NOMENSEIGNANT        varchar(50),
   PRENOMENSEIGNANT     varchar(50),
   CIVILITEENSEIGNANT   varchar(20),
   primary key (IDENSEIGNANT)
);

/*==============================================================*/
/* Table: ETUDIANT                                              */
/*==============================================================*/
create table ETUDIANT
(
   IDETUDIANT           int not null AUTO_INCREMENT, 
   NOMETUDIANT          varchar(50),
   PRENOMETUDIANT       varchar(50),
   primary key (IDETUDIANT)
);

/*==============================================================*/
/* Table: FILIERE                                               */
/*==============================================================*/
create table FILIERE
(
   CODEFILIERE          varchar(8) not null,
   NOMFILIERE           varchar(255),
   primary key (CODEFILIERE)
);

alter table CONVOCATION add constraint FK_CONVOQUER foreign key (IDETUDIANT)
      references ETUDIANT (IDETUDIANT) on delete restrict on update restrict;

alter table CONVOCATION add constraint FK_CONVOQUER2 foreign key (IDCOURS)
      references COURS (IDCOURS) on delete restrict on update restrict;

alter table COURS add constraint FK_DIRIGE foreign key (IDENSEIGNANT)
      references ENSEIGNANT (IDENSEIGNANT) on delete restrict on update restrict;

alter table COURS_FILIERE add constraint FK_APPARTENIR foreign key (IDCOURS)
      references COURS (IDCOURS) on delete restrict on update restrict;

alter table COURS_FILIERE add constraint FK_APPARTENIR2 foreign key (CODEFILIERE)
      references FILIERE (CODEFILIERE) on delete restrict on update restrict;

alter table DOSSIER_SCOLAIRE add constraint FK_AVOIR foreign key (IDETUDIANT)
      references ETUDIANT (IDETUDIANT) on delete restrict on update restrict;

alter table DOSSIER_SCOLAIRE add constraint FK_LIEER foreign key (CODEFILIERE)
      references FILIERE (CODEFILIERE) on delete restrict on update restrict;

