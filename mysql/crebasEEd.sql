/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     5/14/2019 3:57:34 PM                         */
/*==============================================================*/


drop table if exists D_AGE;

drop table if exists D_CSP;

drop table if exists D_ETUDIANT;

drop table if exists D_GEOGRAPHIE;

drop table if exists D_NATIONALITE;

drop table if exists D_PROVENANCE;

drop table if exists D_SERIEBAC;

drop table if exists C_ETUDIANT;

/*==============================================================*/
/* Table: D_AGE                                                 */
/*==============================================================*/
create table D_AGE
(
   AGE                  int not null,
   TRANCHEAGE           varchar(250),
   primary key (AGE)
)ENGINE=InnoDB CHARACTER SET=utf8;

/*==============================================================*/
/* Table: D_CSP                                                 */
/*==============================================================*/
create table D_CSP
(
   CSP                  varchar(250) not null,
   PARENT               varchar(250),
   primary key (CSP)
)ENGINE=InnoDB CHARACTER SET=utf8;

/*==============================================================*/
/* Table: D_ETUDIANT                                            */
/*==============================================================*/
create table D_ETUDIANT
(
   NUMETUDIANT          int not null,
   NOMETUDIANT          varchar(250),
   PRENOMETUDIANT       varchar(250),
   primary key (NUMETUDIANT)
)ENGINE=InnoDB CHARACTER SET=utf8;

/*==============================================================*/
/* Table: D_GEOGRAPHIE                                          */
/*==============================================================*/
create table D_GEOGRAPHIE
(
   VILLE                varchar(250) not null,
   DEPARTEMENT          varchar(250),
   REGION               varchar(250),
   PAYS                 varchar(250),
   primary key (VILLE)
)ENGINE=InnoDB CHARACTER SET=utf8;

/*==============================================================*/
/* Table: D_NATIONALITE                                         */
/*==============================================================*/
create table D_NATIONALITE
(
   NATIONALITE          varchar(250) not null,
   ESTETRANGER          bool,
   primary key (NATIONALITE)
)ENGINE=InnoDB CHARACTER SET=utf8;

/*==============================================================*/
/* Table: D_PROVENANCE                                          */
/*==============================================================*/
create table D_PROVENANCE
(
   PROVENANCE           varchar(250) not null,
   CATEGORIEPROVENANCE  varchar(250),
   primary key (PROVENANCE)
)ENGINE=InnoDB CHARACTER SET=utf8;

/*==============================================================*/
/* Table: D_SERIEBAC                                            */
/*==============================================================*/
create table D_SERIEBAC
(
   SERIEBAC             varchar(250) not null,
   CATEGORIEBAC         varchar(250),
   primary key (SERIEBAC)
)ENGINE=InnoDB CHARACTER SET=utf8;

/*==============================================================*/
/* Table: C_ETUDIANT                                              */
/*==============================================================*/
create table C_ETUDIANT
(
   NUMETUDIANT          int not null,
   ANNEESCOLAIRE        varchar(20) not null,
   CSP                  varchar(250),
   SERIEBAC             varchar(250),
   NATIONALITE          varchar(250),
   VILLE                varchar(250),
   AGE                  int,
   SEXE                 varchar(10),
   NIVEAU               varchar(10),
   FILIERE              varchar(250),
   LANGUEVIVANTE        varchar(50),
   PROVENANCE           varchar(250),
   NBRATTRAPAGE         int,
   primary key (NUMETUDIANT, ANNEESCOLAIRE)
)ENGINE=InnoDB CHARACTER SET=utf8;

alter table C_ETUDIANT add constraint FK_REFERENCE_1 foreign key (AGE)
      references D_AGE (AGE) on delete restrict on update restrict;

alter table C_ETUDIANT add constraint FK_REFERENCE_2 foreign key (CSP)
      references D_CSP (CSP) on delete restrict on update restrict;

alter table C_ETUDIANT add constraint FK_REFERENCE_4 foreign key (NUMETUDIANT)
      references D_ETUDIANT (NUMETUDIANT) on delete restrict on update restrict;

alter table C_ETUDIANT add constraint FK_REFERENCE_5 foreign key (NATIONALITE)
      references D_NATIONALITE (NATIONALITE) on delete restrict on update restrict;

alter table C_ETUDIANT add constraint FK_REFERENCE_6 foreign key (VILLE)
      references D_GEOGRAPHIE (VILLE) on delete restrict on update restrict;

alter table C_ETUDIANT add constraint FK_REFERENCE_7 foreign key (SERIEBAC)
      references D_SERIEBAC (SERIEBAC) on delete restrict on update restrict;

alter table C_ETUDIANT add constraint FK_REFERENCE_8 foreign key (PROVENANCE)
      references D_PROVENANCE (PROVENANCE) on delete restrict on update restrict;

